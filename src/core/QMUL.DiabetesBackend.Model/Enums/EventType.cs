namespace QMUL.DiabetesBackend.Model.Enums;

/// <summary>
/// The health event type: Medication dosage, Insulin dosage, or Measurement
/// </summary>
public enum EventType
{
    MedicationDosage,
    InsulinDosage,
    Measurement
}